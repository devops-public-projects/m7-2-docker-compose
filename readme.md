# Docker Compose | MongoDB and Mongo-Express
In this project we will a Docker Compose file to run our MongoDB and Mongo-Express containers

## Technologies Used
- Docker
- Node.js
- MondoDB
- MongoExpress
- Git
- Linux (Ubuntu)


## Project Description
- Write a Docker Compose file to run MongoDB and MongoExpress containers.

## Prerequisites
- Linux Machine configured with Docker, Git
- The `app` folder from this [repository](https://gitlab.com/twn-devops-bootcamp/latest/07-docker/js-app)

## Guide Steps
### Initial Configuration
- `mkdir ~/repos/m7-2/`
- Put the `app` folder from the above repository in this directory
- `cd app`
- `touch mongo-docker-compose.yaml`
- Modify the commands below into the yaml file
1. `docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password –name mongodb –net mongo-network mongo`

2. `docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password --net mongo-network --name mongo-express -e ME_CONFIG_MONGODB_SERVER=mongodb mongo-express`

- The file will look something like this:
```
{
version: '3'
services:
	mongodb:
		image: mongo
		ports:
			- 27017:27017
}
```

- Save the file and run the command
	- `docker compose -f mongo-docker-compose.yaml up`

![Containers are now running](/images/m7-2-containers-running.png)

- Use the Mongo-Express UI to create our database and collection
	- Web Browser: `localhost:8081`
	- Create Database
		- Name: `user-account`
	- Open `user-account` database
	- Create collection
		- Name: `users`

- Start our app
	- `nohup node server.js &`
	- Web browser: `localhost:3000`
	- Making changes will now save to our Mongodb container

![App is accessible](/images/m7-2-app-accessible.png)


![Database is connected and saving changes](/images/m7-2-app-connects-to-database.png)

- To stop the containers
	- `docker compose -f mongo-docker-compose.yaml down`

### Notes
- If you don't put the `yaml` file inside of the app folder, it will not work correctly and Mongo-Express will throw an authentication error
- Under the Mongo-Express command, add a `restart: always` line.
	- This will allow Mongo-Express to restart until it connects to MongoDB. This can be a problem if Mongo starts after Mongo-Express or Mongo is still starting and isn't finished when Mongo-Express is done.
	- You can alternatively use a`depends_on` line
